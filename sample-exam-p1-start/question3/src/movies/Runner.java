package movies;

public class Runner {
    public static void main(String[] args) {

		Platform netFlix = new Platform();
		netFlix.addMovie(new Movie("Batman", 1989));
		netFlix.addMovie(new Movie("When Harry Met Sally", 1989));
		netFlix.addMovie(new Movie("Dances With Wolves", 1990));
		netFlix.addMovie(new Movie("Pretty Woman", 1990));
		netFlix.addMovie(new Movie("Total Recal", 1990));
		netFlix.addMovie(new Movie("Thelma & Louise", 1991));
		netFlix.addMovie(new Movie("The Silence Of The Lambs", 1991));
		netFlix.addMovie(new Movie("Reservoir Dogs", 1992));
		netFlix.addMovie(new Movie("Jurassic Park", 1993));
		netFlix.addMovie(new Movie("Schindler's List", 1993));


		int filmsBefore1990 = 0;

		for (int i = 0; i < netFlix.getNumberOfMovies(); i++) {
			System.out.printf("%s\n", netFlix.getMovie(i).toString());

			if (netFlix.getMovie(i).getYear() < 1990)
				filmsBefore1990++;
		}

		System.out.printf("There are %d movies from before nineties.\n", filmsBefore1990);
		System.out.printf("The platform has %sroom available for additional movies.", (netFlix.isFull() ? "NO " : ""));


// TODO 3.1 Add the classes Movie and Platform as explained in README.md.
// TODO 3.2 uncomment the code above. If Movie and platform are correct you should not have errors in the above code
// TODO 3.1 Add code below to
//    Print every movie on the netFlix platform.
//    Print the number of movies that are older than 1990
//    Print if netFlix has capacity for additional movies
    }
}
