package movies;

public class Platform {

    private final int CAPACITY = 10;
    private final Movie[] movies = new Movie[CAPACITY];
    private int next;

    public void addMovie(Movie movie) {
        if (next != CAPACITY) {
            movies[next] = movie;
            next++;
        }
        else {
            System.out.println("Array full!");
        }
    }


    Movie getMovie(int index) {
        return movies[index];
    }

    public int getNumberOfMovies() {

        int counter = 0;

        for (Movie movie : movies) {
            if (movie != null)
                counter++;
        }
        return counter;
    }

    public boolean isFull() {
        return getNumberOfMovies() == CAPACITY;
    }
}
